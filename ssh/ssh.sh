#!/bin/bash -x

set -o errexit

ctr1=$(buildah from centos:latest)
mountpoint=$(buildah mount $ctr1)

buildah run $ctr1 yum -y install openssh-server
buildah run $ctr1 bash -c "echo 'root:PqX3fQU5' | chpasswd"
buildah run $ctr1 ssh-keygen -f /etc/ssh/ssh_host_rsa_key -N '' -t rsa
buildah run $ctr1 ssh-keygen -f /etc/ssh/ssh_host_dsa_key -N '' -t dsa

buildah run $ctr1 /sbin/groupadd -g 3345  www
buildah run $ctr1 /sbin/useradd -g 3345 -u 3346 www

buildah config --cmd "/usr/sbin/sshd -D" $ctr1
buildah config --port 22 $ctr1


buildah commit --format docker "$ctr1" "mareksr/ssh"


#clean

buildah rm $ctr1


#push to docker
buildah push localhost/mareksr/ssh docker-daemon:mareksr/ssh:latest
