#!/bin/bash -x

set -o errexit

ctr1=$(buildah from mareksr/c8:2)
mountpoint=$(buildah mount $ctr1)


ctr2=$(buildah from mareksr/httpd:2.4.41)
mountpoint2=$(buildah mount $ctr2)

#copy apache from httpd container
buildah copy $ctr1 $mountpoint2/opt/httpd-2.4.41 /opt/httpd-2.4.41


buildah run $ctr1 sed -i 's/^LoadModule extract_forwarded_module/#LoadModule extract_forwarded_module/' /opt/httpd-2.4.41/conf/httpd.conf
#libpng is in c7 image
: '
buildah config --workingdir /usr/src $ctr1
#https://downloads.sourceforge.net/project/libpng/libpng16/1.6.36/libpng-1.6.36.tar.gz?r=https%3A%2F%2Fsourceforge.net%2Fprojects%2Flibpng%2Ffiles%2Flibpng16%2F1.6.36%2Flibpng-1.6.36.tar.gz%2Fdownload%3Fuse_mirror%3Dnetix%26download%3D&ts=1553158660
buildah run $ctr1  wget 'https://bit.ly/2JpMV2P' -O libpng-1.6.36.tar.gz
buildah config --workingdir /usr/src $ctr1
buildah run $ctr1 tar -vxf  libpng-1.6.36.tar.gz
buildah config --workingdir /usr/src/libpng-1.6.36 $ctr1
buildah run $ctr1 ./configure --prefix=/usr/local/libpng-1.6.36 --enable-shared
buildah run $ctr1 make
buildah run $ctr1 make install
'

buildah config --workingdir /usr/src $ctr1
buildah run $ctr1  wget http://museum.php.net/php4/php-4.4.9.tar.gz -O php-4.4.9.tar.gz



buildah config --workingdir /usr/src $ctr1
buildah run $ctr1 tar -vxf  php-4.4.9.tar.gz


buildah config --workingdir /usr/src/php-4.4.9 $ctr1

#API 2.2 -> 2.4 change
buildah run $ctr1 sed -i 's/unixd_config\./ap_unixd_config\./g' sapi/apache2handler/php_functions.c
buildah run $ctr1 sed -i 's/ap_get_server_version/ap_get_server_banner/g' sapi/apache2handler/php_functions.c


buildah run $ctr1 ./configure '--prefix=/opt/php-4.4.9' --with-apxs2=/opt/httpd-2.4.41/bin/apxs  '--enable-exif' '--enable-gd-native-ttf' \
 '--with-xsl' '--with-mysql=shared'   '--enable-mbstring' '--with-gd' '--with-zlib'  '--with-pdo-mysql' '--with-mysqli=shared' \
 '--with-freetype-dir' '--with-curl'  '--enable-soap'  '--enable-zip=/usr/local/lib64/' '--enable-ftp' '--with-gettext' '--with-mcrypt=/opt/libmcrypt-2.5.8' '--enable-bcmath' '--enable-intl' '--with-png-dir=/usr/local/libpng-1.6.36'

buildah run $ctr1 make
buildah run $ctr1 make install

#and here comes the mouse
ctr3=$(buildah from centos:centos7.7.1908)
buildah config --label maintainer="Marek Sroczynski <mareksr@hm.pl>" $ctr3

buildah copy $ctr3 $mountpoint/opt/php-4.4.9 /opt/php-4.4.9
buildah copy $ctr3 $mountpoint/opt/httpd-2.4.41 /opt/httpd-2.4.41
buildah copy $ctr3 $mountpoint/opt/apr-util-1.6.1 /opt/apr-util-1.6.1
buildah copy $ctr3 $mountpoint/opt/apr-1.7.0 /opt/apr-1.7.0
buildah copy $ctr3 $mountpoint/opt/libmcrypt-2.5.8 /opt/libmcrypt-2.5.8
buildah copy $ctr3 $mountpoint/usr/local/libpng-1.6.36 /usr/local/libpng-1.6.36
buildah copy $ctr3 $mountpoint/lib64/libfreetype.so* /lib64/
buildah copy $ctr3 $mountpoint/lib64/libpng* /lib64/



buildah run $ctr3 mkdir -p /usr/local/Zend2/lib/ioncube
buildah copy $ctr3 $mountpoint2/usr/src/ioncube /usr/local/Zend2/lib/ioncube

buildah copy $ctr3 $mountpoint/usr/sbin/ssmtp /usr/sbin/ssmtp
buildah run $ctr3 ln -s /usr/sbin/ssmtp /usr/sbin/sendmail

buildah run $ctr3 ln -s /opt/php-4.4.9 /usr/local/php4
buildah run $ctr3 ln -s  /opt/httpd-2.4.41 /usr/local/apache2


buildah run $ctr3 /sbin/groupadd -g 80 www
buildah run $ctr3 /sbin/useradd -g 80 -u 80 www
buildah run $ctr3 mkdir /logs
#
buildah config --cmd "/usr/local/apache2/bin//httpd -D FOREGROUND" $ctr3
buildah config --port 80 "$ctr3"

#do it !
buildah commit --format docker "$ctr3" "mareksr/php4"


#clean
buildah unmount $ctr3
buildah rm $ctr3

buildah unmount $ctr2
buildah rm $ctr2

buildah unmount $ctr1
buildah rm $ctr1

#push to docker
buildah push localhost/mareksr/php4 docker-daemon:mareksr/php4:4.4.9

