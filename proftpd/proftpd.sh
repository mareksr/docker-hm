#!/bin/bash -x

set -o errexit

ctr1=$(buildah from mareksr/c7)
mountpoint=$(buildah mount $ctr1)



buildah config --workingdir /usr/src $ctr1
buildah run $ctr1 wget ftp://ftp.proftpd.org/distrib/source/proftpd-1.3.6.tar.gz -O proftpd-1.3.6.tar.gz

buildah config --workingdir /usr/src $ctr1
buildah run $ctr1 tar -vxf   proftpd-1.3.6.tar.gz

buildah config --workingdir /usr/src/proftpd-1.3.6 $ctr1
buildah run $ctr1 ./configure --prefix=/opt/proftpd-1.3.6 --with-modules=mod_tls:mod_wrap2:mod_wrap2_file

buildah run $ctr1 make
buildah run $ctr1 make install



#and here comes the mouse
ctr3=$(buildah from centos)
buildah config --label maintainer="Marek Sroczynski <mareksr@hm.pl>" $ctr3
buildah copy $ctr3 $mountpoint/opt/proftpd-1.3.6 /opt/proftpd-1.3.6

buildah run $ctr3 /sbin/groupadd -g 80 www
buildah run $ctr3 /sbin/useradd -g 80 -u 80 -b /var www

buildah run $ctr3 mkdir -p /var/log/proftpd


buildah config --port 20 "$ctr3"
buildah config --port 21 "$ctr3"
buildah config --port 49152 "$ctr3"
buildah config --port 49153 "$ctr3"
buildah config --port 49154 "$ctr3"
buildah config --port 49155 "$ctr3"


buildah config --cmd "/opt/proftpd-1.3.6/sbin/proftpd  --nodaemon -c /opt/proftpd-1.3.6/etc/proftpd.conf" $ctr3


buildah commit --format docker "$ctr3" "mareksr/proftpd"

buildah unmount $ctr1
buildah rm $ctr1

buildah unmount $ctr3
buildah rm $ctr3

#push to docker
buildah push localhost/mareksr/proftpd docker-daemon:mareksr/proftpd:latest