#!/bin/bash -x

set -o errexit

ctr1=$(buildah from mareksr/c7)
mountpoint=$(buildah mount $ctr1)

buildah config --workingdir /usr/src $ctr1
buildah run $ctr1 wget https://www.bacula.org/download/7471/ -O bacula-9.4.2.tar.gz

buildah config --workingdir /usr/src $ctr1
buildah run $ctr1 tar -vxf  bacula-9.4.2.tar.gz

buildah config --workingdir /usr/src/bacula-9.4.2 $ctr1

buildah run $ctr1 ./configure --prefix=/opt/bacula-9.4.2 --enable-client-only
buildah run $ctr1 make 
buildah run $ctr1 make install



#and here comes the mouse
ctr2=$(buildah from centos)
buildah config --label maintainer="Marek Sroczynski <mareksr@hm.pl>" $ctr2
buildah copy $ctr2 $mountpoint/opt/bacula-9.4.2 /opt/bacula-9.4.2


buildah config --port 9102 "$ctr2"
buildah config --cmd "/opt/bacula-9.4.2/sbin/bacula-fd -f" $ctr2


buildah commit --format docker "$ctr2" "mareksr/bacula"

buildah unmount $ctr1
buildah rm $ctr1

buildah unmount $ctr2
buildah rm $ctr2

#push to docker
buildah push localhost/mareksr/bacula docker-daemon:mareksr/bacula:latest
