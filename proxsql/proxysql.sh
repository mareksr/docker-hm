#!/bin/bash -x

set -o errexit

ctr1=$(buildah from centos)
mountpoint=$(buildah mount $ctr1)

buildah run $ctr1  /bin/bash -c "echo alias padmin=\'mysql -u admin -padmin -h 127.0.0.1 -P6032 --prompt=Admin.\' >> ~/.bashrc"

buildah run $ctr1 yum -y localinstall https://github.com/sysown/proxysql/releases/download/v2.0.3/proxysql-2.0.3-1-centos7.x86_64.rpm
buildah run $ctr1 yum -y install mysql


buildah config --cmd "/usr/bin/proxysql -f" $ctr1
buildah config --port 3306 "$ctr1"


buildah commit --format docker "$ctr1" "mareksr/proxysql"
buildah push localhost/mareksr/proxysql docker-daemon:mareksr/proxysql:latest
