#!/bin/bash -x

set -o errexit

ctr1=$(buildah from mareksr/c8:2)
mountpoint=$(buildah mount $ctr1)


ctr2=$(buildah from mareksr/httpd-2.4.41)
mountpoint2=$(buildah mount $ctr2)

buildah run $ctr1 yum -y install mariadb-devel mariadb-libs mariadb-server libpng-devel libjpeg-devel libtomcrypt-devel
#copy apache from httpd container
buildah copy $ctr1 $mountpoint2/opt/httpd-2.4.41 /opt/httpd-2.4.41


buildah run $ctr1 sed -i 's/^LoadModule extract_forwarded_module/#LoadModule extract_forwarded_module/' /opt/httpd-2.4.41/conf/httpd.conf


buildah config --workingdir /usr/src $ctr1
buildah run $ctr1 wget http://museum.php.net/php5/php-5.2.17.tar.gz -O php-5.2.17.tar.gz

buildah config --workingdir /usr/src $ctr1
buildah run $ctr1 wget "https://storage.googleapis.com/google-code-archive-downloads/v2/code.google.com/php52-backports/php52-backports-security-20130717.patch" -O php52-backports-security-20130717.patch




buildah config --workingdir /usr/src $ctr1
buildah run $ctr1 tar -vxf  php-5.2.17.tar.gz


buildah config --workingdir /usr/src/php-5.2.17 $ctr1

#API 2.2 -> 2.4 change
buildah run $ctr1 sed -i 's/unixd_config\./ap_unixd_config\./g' sapi/apache2handler/php_functions.c
buildah run $ctr1 sed -i 's/ap_get_server_version/ap_get_server_banner/g' sapi/apache2handler/php_functions.c

buildah run $ctr1 bash -c 'patch -p1 < /usr/src/php52-backports-security-20130717.patch'
buildah run $ctr1 bash -c 'cp -Rp /usr/local/libpng-1.6.36/lib/* /usr/lib64'



buildah run $ctr1 ./configure '--prefix=/opt/php-5.2.17' --with-apxs2=/opt/httpd-2.4.41/bin/apxs  '--enable-exif' '--enable-gd-native-ttf'  \
 '--with-xsl' '--with-mysql=shared'   '--enable-mbstring' '--with-gd' '--with-zlib'  '--with-pdo-mysql' '--with-mysqli=shared' \
 '--with-freetype-dir' '--with-curl'  '--enable-soap'  '--enable-zip=/usr/local/lib64/' '--enable-ftp' '--with-gettext'  '--enable-bcmath' '--enable-intl' --with-libdir=lib64


buildah run $ctr1 make
buildah run $ctr1 make install

#and here comes the mouse
ctr3=$(buildah from centos:centos7.7.1908)
buildah config --label maintainer="Marek Sroczynski <mareksr@hm.pl>" $ctr3

buildah copy $ctr3 $mountpoint/opt/php-5.2.17 /opt/php-5.2.17
buildah copy $ctr3 $mountpoint/opt/httpd-2.4.41 /opt/httpd-2.4.41
buildah copy $ctr3 $mountpoint/opt/apr-util-1.6.1 /opt/apr-util-1.6.1
buildah copy $ctr3 $mountpoint/opt/apr-1.7.0 /opt/apr-1.7.0
buildah copy $ctr3 $mountpoint/opt/libmcrypt-2.5.8 /opt/libmcrypt-2.5.8
buildah copy $ctr3 $mountpoint/usr/local/libpng-1.6.36 /usr/local/libpng-1.6.36
buildah copy $ctr3 $mountpoint/lib64/libfreetype.so* /lib64/
buildah copy $ctr3 $mountpoint/lib64/libpng* /lib64/
buildah copy $ctr3 $mountpoint/lib64/libexslt.so* /lib64/
buildah copy $ctr3 $mountpoint/usr/lib64/mysql /usr/lib64/mysql
buildah copy $ctr3 $mountpoint/lib64/libxslt.so* /lib64
buildah copy $ctr3 $mountpoint/lib64/libnsl* /lib64
buildah copy $ctr3 $mountpoint/lib64/libssl* /lib64


buildah copy $ctr3 $mountpoint/usr/sbin/ssmtp /usr/sbin/ssmtp
buildah run $ctr3 ln -s /usr/sbin/ssmtp /usr/sbin/sendmail

buildah run $ctr3 mkdir -p /usr/local/Zend2/lib/ioncube
buildah copy $ctr3 $mountpoint2/usr/src/ioncube /usr/local/Zend2/lib/ioncube


buildah run $ctr3 ln -s /opt/php-5.2.17 /usr/local/php5
buildah run $ctr3 ln -s /opt/httpd-2.4.41/bin/httpd /opt/httpd-2.4.41/bin/httpd72
buildah run $ctr3 ln -s  /opt/httpd-2.4.41 /usr/local/apache2


buildah run $ctr3 /sbin/groupadd -g 80 www
buildah run $ctr3 /sbin/useradd -g 80 -u 80 www
buildah run $ctr3 mkdir /logs
#
buildah config --cmd "/usr/local/apache2/bin//httpd -D FOREGROUND" $ctr3
buildah config --port 80 "$ctr3"

#do it !
buildah commit --format docker "$ctr3" "mareksr/php52"


#clean
buildah unmount $ctr3
buildah rm $ctr3

buildah unmount $ctr2
buildah rm $ctr2

buildah unmount $ctr1
buildah rm $ctr1

#push to docker
buildah push localhost/mareksr/php52 docker-daemon:mareksr/php:5.2.17

